#You should specify your prefered region where your ec2 instance will be created
region="eu-west-1"
#You should find this ID in the AWS Console
ami_id="ami-0aef57767f5404a3c"
#This must be created from the AWS Console
my_key_pair="terraform_key"