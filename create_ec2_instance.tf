##################################################################
# Author : walid.yaich@gmail.com
#
# If this is your first terraform project :
# 1) Install terraform
# 2) Install and configure AWS CLI 
# 3) terraform init
# 4) terraform fmt
# 5) terraform validate
#
# This project must  : 
# 1) Create a Security Group which allow SSH connection from my public IP address.
# 2) Create an EC2 instance from the specified AMI in the default VPC of the specified region (see terraform.tvars).
# 3) Assign the created security group to the EC2 instance.
# 4) Assign your key pair (already created from AWS Console) to the EC2 instance.
# 5) After "terraform apply", it should output the ec2 instance public IP address, this IP will be used to ssh into the EC2 instance
#  
# How to test :
# 1) terraform validate
# 2) terraform apply
# 3) In your shell (tested on linux) :
#       cd /path/to/your/key_pair 
#       chmod 400 <your_key_pair.pem>
#       ssh -i <your_key_pair.pem> ubuntu@<the outputted ip address>
# 4) If you're inside the EC2 instance, congrats !!!  otherwise you need to do some troubleshooting .... 
# 5) After finishing, please run "terraform destroy" to remove all created ressources (so you won't waste money) ==> so easy to cleanup with terraform :)
###################################################################

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws" #To download the aws provider for terraform 
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default" #To read credentials from default location
  region  = var.region
}

#To get my public IP Address, i need this to allow port 22 for my ip address, this is a security best practice !
data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow ssh inbound traffic for my IP address"

  ingress {
    description = "SSH from my public ip address"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.body)}/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_instance" "ubuntu_ec2_instance" {
  ami           = var.ami_id  #Ubuntu 20.04, ami ID is copied from aws console
  instance_type = "t2.micro"
  security_groups = [ "allow_ssh" ]
  key_name = var.my_key_pair
}

output "ip" {
  value = aws_instance.ubuntu_ec2_instance.public_ip
}