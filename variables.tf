variable "region" {
  type = string
}

variable "ami_id" {
  type = string
}

variable "my_key_pair" {
  type = string
}
